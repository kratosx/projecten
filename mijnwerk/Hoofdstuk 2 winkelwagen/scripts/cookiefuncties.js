// FUNCTION: maakCookie
// PARAMETERS: name, value, amount of days
// RETURNS: Cookie
// PURPOSE: Create a cookie
function maakCookie(naam, waarde, dagen){
    if(dagen){
        var datum = new Date();
        datum.setTime(datum.getTime() + (dagen*24*60*60*1000));
        var verloopdatum = `; expires= ${datum.toGMTString()}`;
    }
    else{
        verloopdatum = ' ';
    }
    document.cookie = naam + '='+waarde+verloopdatum+'path=/';

}// end of maakCookie

// FUNCTION: leesCookie
// PARAMETERS: name of the cookie you want to read
// RETURNS: cookie value in string format
// PURPOSE: Showing the values of a cookie
function leesCookie(naam){
    let naamCookie = naam + '=';
    let cookieArray = document.cookie.split(';');
    
    for(let i=0; i < cookieArray.length; i++){
        let dezeCookie = cookieArray[i];
        dezeCookie = dezeCookie.trim();

        if(dezeCookie.indexOf(naamCookie) == 0){
            return dezeCookie.substring(naamCookie.length, dezeCookie.length);
        } 
    }
    return null;
}// end of leesCookie

// FUNCTION: verwijderAlleCookies
// PARAMETERS: NULL
// RETURNS: NULL
// PURPOSE: removing all Cookies
function verwijderAlleCookies(){
    // maak een array van elke cookie en verwijder deze allemaal
    let cookieArray = document.cookie.split(';');

    // elke cookie zit nu op zijn eigen index, hiervan pak ik de naam en voer ik de functie maakCookie uit met experation date -1;
    for(let i=0; i<cookieArray.length; i++){
        let dezeCookie = cookieArray[i];
        dezeCookie = dezeCookie.trim();
        let cookieNaamLengte = dezeCookie.indexOf("=")
        let cookieNaam = dezeCookie.substring(0, cookieNaamLengte);
        
        maakCookie(cookieNaam, " ", -1);
    }
}

// FUNCTION: verwijderCookie
// PARAMETERS: Name of the cookie you want to delete
// RETURNS: NULL
// PURPOSE: Deleting a specific cookie
function verwijderCookie(cookieNaam){
    maakCookie(cookieNaam, " ", -1)
}