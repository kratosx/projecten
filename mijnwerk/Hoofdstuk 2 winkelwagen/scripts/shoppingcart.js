// FUNCTION: bestellen
// PARAMETERS: Form Object
// RETURNS: Cookie met bestelling info...
// PURPOSE: cookie maken met info bestelde item
function bestellen(thisForm){
    productInfo = 
    thisForm.id.value + "|" +
    thisForm.merk.value + "|" +
    thisForm.model.value + "|" + 
    thisForm.geheugen.value + "|" +
    thisForm.prijs.value + "|" +
    thisForm.aantal.value + "|" +
    (thisForm.prijs.value * thisForm.aantal.value) + "|";

    maakCookie(thisForm.id.value, productInfo, 1);


    notice = `${thisForm.aantal.value} ${thisForm.merk.value} in winkelwagen.`
    alert(notice);
}


// FUNCTION: cartWeergaven
// PARAMETERS: null
// RETURNS: Productentabel
// PURPOSE: geeft productentabel weer in html formaat
function cartWeergeven(){
    tabelrij = '';
    for(let i = 0; i <= 6; i++){
        let dezeCookie = leesCookie(i);
        if(dezeCookie == null || dezeCookie == 'niet-gevonden'){
            continue;
        }
        velden = [];
        velden = dezeCookie.split('|');
        tabelrij +=
        `<tr>
            <td>${velden[0]}</td>
            <td>${velden[1]}</td>
            <td>${velden[2]}</td>
            <td>${velden[3]}</td>
            <td>${velden[4]}</td>
            <td>${velden[5]}</td>
            <td>${velden[6]}</td>
            <td><img src="../images/delete.png" onclick=
            "verwijderCookie(${velden[0]}); location.reload()"></td>
        </tr>`;
    };
    document.write(tabelrij);
}

