<?php include("header.php"); ?>
<html lang="nl">

<head>
	<title>Doel van website</title>
	<!-- Stylesheet -->
	<link rel="stylesheet" href="css/style.css">
</head>

<body>
	<!-- in pre kan je langere teksten plaatsen zonder elke keer <br> te hoeven doen. -->
	<div class="container">
		<div class="textbox-header">
			<h1>Doel van deze website</h1>
		</div>
		<div class="textbox">
			<p>
				Het doel is om u, de lezer, te informeren over een precieze verzameling van <?php fileCount('talen/'); ?> verschillende programmeertalen en u iets bij te leren over de geschiedenis en het moderne gebruik ervan binnen de wereld van technologie en programmeren.
				We hebben een lijst samengesteld met informatie die volgens ons het meest relevant is als het gaat om kennismaken met de basis programmeertalen en hebben het leesmateriaal iets compacter gemaakt. We hebben ervoor gezorgd dat onze bronnen betrouwbaar en up-to-date zijn met wat ze beweren.
				Het specifieke nummer 42 is een directe verwijzing naar: The hitchhiker’s guide to the galaxy.
			</p>
		</div>
	</div>
	<?php include("footer.php");?>
</body>
</html>