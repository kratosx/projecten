<?php include("../header-talen.php");?>
    <div class="container">
            <div class="contInfo">
                    <img class="contImg" src="../img/brainfuck.png" alt="Brainfuck Logo">
                <div class="contOpsom">
                    <ul>
                        <li><b>Verschenen:</b> 1993 </li>
                        <li><b>Ontwikkeld door:</b> Urban Müller </li>
                        <li><b>Paradigma:</b> Esoterisch, imperatief, gestructureerd </li>
                        <li><b>Huidige versie:</b> - </li>
                        <li><b>Generatie:</b> Derde </li>
                        <li><b>Zie ook: </b>        
                        <div class="btn-group">
                            <a href="https://nl.wikipedia.org/wiki/Brainfuck"><button class="button">Wikipedia</button></a>
                            <a href="../generatie.php#derde"><button class="button">Generaties</button></a>
                            <a href="../paradigma.php"><button class="button">Paradigma's</button></a>
                        </div>
                    </ul>
                </div>
            </div>
        <div class="contBox">
            <h1>Brainfuck</h1>
            <p>
                Müllers doel was om een turing-volledige programmeertaal te maken die met de kleinst mogelijke compiler zou kunnen worden geïmplementeerd. De taal bestaat uit acht statements. De tweede versie van de originele compiler[1], geschreven voor de Amiga, is slechts 240 bytes groot. Urban werd geïnspireerd door de programmeertaal False, waarvan de compiler 1024 bytes groot was.
                Zoals de naam al suggereert, zijn brainfuck-programma's over het algemeen moeilijk te begrijpen. Daar staat tegenover dat elke turingmachine, en daardoor ook brainfuck, elke rekentaak kan volbrengen. Als we de moeilijkheden om bepaalde taken te programmeren buiten beschouwing laten, is dat inderdaad mogelijk.
                De taal is gebaseerd op een zeer eenvoudige virtuele machine die buiten het programma bestaat uit een rij van bytes die op nul zijn gezet, een pointer naar een element van de rij (de startwaarde van de pointer is de eerste byte) en twee rijen bytes die de invoer en uitvoer vormen. Brainfuck-code kan eenvoudig als C-code herschreven worden
            </p>
            
        </div>

    </div>
    <?php include("../footer-talen.php");?>
</body>
</html>