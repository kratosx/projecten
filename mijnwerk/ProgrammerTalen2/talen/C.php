<?php include("../header-talen.php");?>
    <div class="container">
            <div class="contInfo">
                    <img class="contImg" src="../img/c.png" alt="c logo">
                <div class="contOpsom">
                    <ul>
                        <li><b>Verschenen:</b><br> 1973 </li>
                        <li><b>Ontwikkeld door:</b><br> Dennis Ritchie en Bell Labs </li>
                        <li><b>Paradigma:</b><br> Imperatief </li>
                        <li><b>Huidige versie:</b><br> C18 </li>
                        <li><b>Generatie:</b><br> Derde</li>
                        <li><b>Zie ook: </b>        
                        <div class="btn-group">
                            <a href="https://www.cprogramming.com/"><button class="button">C programming</button></a>
                            <a href="../generatie.php#derde"><button class="button">Generaties</button></a>
                            <a href="../paradigma.php"><button class="button">Paradigma's</button></a>
                        </div>
                    </ul>
                </div>
            </div>
        <div class="contBox">
            <h1>C</h1>
            <p>
                De programmeertaal C is een imperatieve programmeertaal, gebaseerd op de programmeertaal B, die zelf weer op BCPL was gebaseerd. Het is een zeer praktische programmeertaal die meer op Algol lijkt dan op andere voorlopers, zoals - in historische volgorde - Fortran, Lisp, COBOL en BASIC.
                Ook Pascal is een versimpeling van Algol, maar dan in een andere richting. Terwijl Pascal meer afstand neemt van de machine waar het op moet werken, ligt C juist dicht tegen de machine aan; het is betrekkelijk 'low-level'.
                De invloed van C is zo groot dat sindsdien de meeste nieuwe talen zoals C++, Objective-C, Java, JavaScript, C# en PHP grotendeels de syntaxis van C gebruiken.
                C heeft getypeerde data, maar maakt conversie wel mogelijk. Conversie kan door middel van zogenaamde casts, de datatypes hoeven niet van dezelfde grootte te zijn, hoewel dit wel aan te raden is. De datarepresentatie van een type op de machine wordt niet door de taal voorgeschreven; een variabele van het type int (een verkorting van het Engelse integer, hetgeen "geheel getal" betekent) kan dus tussen verschillende machines verschillen en bijvoorbeeld 16 of 32 bits lang zijn.
            </p>
            
        </div>

    </div>
    <?php include("../footer-talen.php");?>
</body>
</html>