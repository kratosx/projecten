<?php include("../header-talen.php");?>
    <div class="container">
            <div class="contInfo">
                    <img class="contImg" src="../img/javascript.jpg" alt="Javascrpit logo">
                <div class="contOpsom">
                    <ul>
                        <li><b>Verschenen:</b> 1995 </li>
                        <li><b>Ontwikkeld door:</b> Brendan Eich </li>
                        <li><b>Paradigma:</b> Multi-paradigma, functioneel, objectgeoriënteerd </li>
                        <li><b>Huidige versie:</b> ECMAScript 2018[ </li>
                        <li><b>Generatie:</b> Derde </li>
                        <li><b>Zie ook: </b>        
                        <div class="btn-group">
                            <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript"><button class="button">Javascript</button></a>
                            <a href="../generatie.php#derde"><button class="button">Generaties</button></a>
                            <a href="../paradigma.php"><button class="button">Paradigma's</button></a>
                        </div>
                    </ul>
                </div>
            </div>
        <div class="contBox">
            <h1>JavaScript</h1>
            <p>
                De eerste versie van JavaScript werd in 1995 ontwikkeld door Brendan Eich van Netscape Communications Corporation voor gebruik in Netscape Navigator. Aanvankelijk was de naam Mocha en vervolgens LiveScript. De taal werd hernoemd tot JavaScript in de tijd dat in de Netscape-browser ook ondersteuning voor Java-applets werd ingebouwd.
                Met de introductie van JavaScript ontstonden de eerste mogelijkheden om webpagina's interactief te maken. Dit werd later Dynamic HTML genoemd.
                Netscape heeft in een poging om JavaScript als een officiële norm erkend te krijgen en er toch zelf voldoende zeggingskracht over te behouden de taal laten normeren door de European Computer Manufacturers Association (ECMA), waarbij de naam om redenen betreffende het merkrecht is gewijzigd in ECMAScript. De relevante ISO-standaard is ISO 16262 (Internationale Organisatie voor Standaardisatie). De huidige versie van JavaScript is versie ECMAScript 2018 (juni 2018).
                Microsoft heeft een eigen implementatie van JavaScript ontwikkeld onder de naam JScript en Adobe heeft een eigen versie genaamd ActionScript die gebruikt wordt in Flash.
                Het gebruik van JavaScript kent weer een opleving als onderdeel van AJAX-toepassingen.
            </p>
            
        </div>

    </div>
    <?php include("../footer-talen.php");?>
</body>
</html>