<?php include("../header-talen.php");?>
    <div class="container">
            <div class="contInfo">
                    <img class="contImg" src="../img/piet.jpg" alt="Piet logo">
                <div class="contOpsom">
                    <ul>
                        <li><b>Verschenen:</b> 1993 </li>
                        <li><b>Ontwikkeld door:</b> David Morgan-Mar </li>
                        <li><b>Paradigma:</b> Esoterisch </li>
                        <li><b>Huidige versie:</b> - </li>
                        <li><b>Generatie:</b> Derde </li>
                        <li><b>Zie ook: </b>        
                        <div class="btn-group">
                            <a href="	http://www.dangermouse.net/esoteric/piet.html"><button class="button">Piet</button></a>
                            <a href="../generatie.php#derde"><button class="button">Generaties</button></a>
                            <a href="../paradigma.php"><button class="button">Paradigma's</button></a>
                        </div>
                    </ul>
                </div>
            </div>
        <div class="contBox">
            <h1>Piet</h1>
            <p>
                Piet is een door David Morgan-Mar ontwikkelde, esoterische programmeertaal waarin computerprogramma's eruitzien als abstracte schilderijen. De taal is vernoemd naar de Nederlandse kunstschilder Piet Mondriaan. Een programma in Piet is een bitmap die door een Piet-interpreter uitgevoerd kan worden.
                De Piet-interpreter maakt gebruik van een stack voor het bijhouden van waarden. De kleuren in een programma worden geïnterpreteerd met behulp van de HSL-kleurruimte (hue, saturation en lightness). Afhankelijk van de overgang in hue en lightness tussen twee pixels in een Piet-programma voert de interpreter een actie uit, zoals het toevoegen van een waarde op de stack, het vergelijken van twee waarden of het uitvoeren van een rekenkundige bewerking. Een pixel in een Piet-programma wordt ook wel een codel genoemd om verwarring te voorkomen bij uitvergrote programma's, aangezien een gekleurd blok dan overeenkomt met meerdere pixels op het beeldscherm.
                De Piet-interpreter houdt tijdens het uitvoeren van een programma ook twee richtingen bij, een zogeheten Direction Pointer (DP) en een Codel Chooser (CC). De uitvoering van het programma begint bij de codel linksbovenaan en met behulp van de Direction Pointer en Codel Chooser wordt bepaald welke volgende codel bekeken wordt.
            </p>
            
        </div>

    </div>
    <?php include("../footer-talen.php");?>
</body>
</html>