<?php include("../header-talen.php");?>
    <div class="container">
            <div class="contInfo">
                    <img class="contImg" src="../img/Smalltalk.jpg" alt="Smalltalk">
                <div class="contOpsom">
                    <ul>
                        <li><b>Verschenen:</b> 1980 </li>
                        <li><b>Ontwikkeld door:</b> Alan Kay, Dan Ingalls, Adele Goldberg, Xerox Parc </li>
                        <li><b>Paradigma:</b> Objectgeoriënteerd </li>
                        <li><b>Huidige versie:</b> Smalltalk-80 version 2 </li>
                        <li><b>Generatie:</b> Derde </li>
                        <li><b>Zie ook: </b>        
                        <div class="btn-group">
                            <a href="http://www.smalltalk.org/"><button class="button">Smalltalk</button></a>
                            <a href="../generatie.php#derde"><button class="button">Generaties</button></a>
                            <a href="../paradigma.php"><button class="button">Paradigma's</button></a>
                        </div>
                    </ul>
                </div>
            </div>
        <div class="contBox">
            <h1>Smalltalk</h1>
            <p>
                Smalltalk is een objectgeoriënteerde programmeertaal met dynamische typen, die ontwikkeld werd bij Xerox PARC door Alan Kay, Dan Ingalls, Ted Kaehler, Adele Goldberg en anderen in de jaren zeventig. De taal werd oorspronkelijk uitgebracht als Smalltalk-80 en wordt sindsdien in brede kring gebruikt.
                Smalltalk wordt voortdurend verder ontwikkeld en rondom de taal is een trouwe gebruikersgemeenschap ontstaan. Smalltalk heeft grote invloed gehad op de ontwikkeling van vele andere programmeertalen, onder andere Objective C, Java en Ruby. De invloed op die laatste is zelfs zo groot dat sommige Smalltalkers Ruby beschouwen als Smalltalk met een andere syntaxis. Veel programmeerconcepten uit de jaren negentig ontstonden in de Smalltalk-gemeenschap, bijvoorbeeld de toepassing van het concept 'design patterns' op software, Extreme Programming en refactoring. Een bekend Smalltalker is Ward Cunningham, de uitvinder van het WikiWiki-concept.
            </p>
            
        </div>

    </div>
    <?php include("../footer-talen.php");?>
</body>
</html>