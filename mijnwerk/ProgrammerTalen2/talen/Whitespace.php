<?php include("../header-talen.php");?>
    <div class="container">
            <div class="contInfo">
                    <img class="contImg" src="../img/Whitespace.png" alt="Whitespace logo">
                <div class="contOpsom">
                    <ul>
                        <li><b>Verschenen:</b> 1 April 2003 </li>
                        <li><b>Ontwikkeld door:</b> Edwin Brady </li>
                        <li><b>Paradigma:</b> Esoterisch </li>
                        <li><b>Huidige versie:</b> - </li>
                        <li><b>Generatie:</b> Derde </li>
                        <li><b>Zie ook: </b>        
                        <div class="btn-group">
                            <a href="https://web.archive.org/web/20080331083700/http://compsoc.dur.ac.uk/whitespace/"><button class="button">Whitespace</button></a>
                            <a href="../generatie.php#derde"><button class="button">Generaties</button></a>
                            <a href="../paradigma.php"><button class="button">Paradigma's</button></a>
                        </div>
                    </ul>
                </div>
            </div>
        <div class="contBox">
            <h1>Whitespace</h1>
            <p>
                Whitespace is een esoterische programmeertaal met broncode die alleen bestaat uit spaties, tabs en regelovergangen (newlines). In veel andere talen hebben deze tekens geen betekenis, behalve om andere elementen van elkaar te scheiden. De taal is ontwikkeld door Edwin Brady en Chris Morris aan de Universiteit van Durham en het werd uitgebracht op 1 april 2003.
                Whitespace is een imperatieve programmeertaal die uitgevoerd wordt door een virtuele machine met een stack en een heap. De programmeur kan gehele getallen met arbitraire grootte op de stack duwen. De heap kan gebruikt worden als permanente opslag voor variabelen en datastructuren.
                Tekens die geen spaties, tabs of regelovergangen zijn, kunnen als commentaar gebruikt worden.
            </p>
            
        </div>

    </div>
    <?php include("../footer-talen.php");?>
</body>
</html>