<?php include("../header-talen.php");?>
    <div class="container">
            <div class="contInfo">
                    <img class="contImg" src="../img/wolfram.png" alt="Wolfram logo">
                <div class="contOpsom">
                    <ul>
                        <li><b>Verschenen:</b><br> 1988 </li>
                        <li><b>Ontwikkeld door:</b><br> Wolfram Research </li>
                        <li><b>Paradigma:</b><br> Multi-paradigma: term-herschrijven, functioneel, procedureel, array </li>
                        <li><b>Huidige versie:</b><br> 12.0 </li>
                        <li><b>Generatie:</b> Derde </li>
                        <li><b>Zie ook: </b>        
                        <div class="btn-group">
                            <a href="https://www.wolfram.com/"><button class="button">Wolfram</button></a>
                            <a href="../generatie.php#derde"><button class="button">Generaties</button></a>
                            <a href="../paradigma.php"><button class="button">Paradigma's</button></a>
                        </div>
                    </ul>
                </div>
            </div>
        <div class="contBox">
            <h1>Wolfram Language</h1>
            <p>
                De Wolfram-taal is een algemene rekentaal met meerdere paradigma's ontwikkeld door Wolfram Research en is de programmeertaal van het wiskundige symbolische berekeningsprogramma Mathematica en de Wolfram Programming Cloud. Het benadrukt symbolische berekening, functioneel programmeren en op regels gebaseerd programmeren en kan willekeurige structuren en gegevens gebruiken.
                Het bevat ingebouwde functies voor het genereren en uitvoeren van Turing-machines, het maken van afbeeldingen en audio, het analyseren van 3D-modellen, matrixmanipulaties en het oplossen van differentiaalvergelijkingen. Het is uitgebreid gedocumenteerd.
                De kernprincipes van Wolfram Language die het onderscheiden van andere programmeertalen omvatten een ingebouwde kennisbank, automatisering in de vorm van meta-algoritmen en superfuncties, een coherent elegant ontwerp en structuur, ingebouwd natuurlijk taalbegrip en weergave van alles als symbolisch uitdrukking.
                De Wolfram Language is in 2013 uitgebracht voor de Raspberry Pi met als doel het gratis te maken voor alle Raspberry Pi-gebruikers. Het werd opgenomen in de aanbevolen softwarebundel die de Raspberry Pi Foundation voor beginners biedt, wat enige controverse veroorzaakte vanwege de eigen aard van de Wolfram-taal. Plannen om de Wolfram-taal over te brengen naar de Intel Edison werden aangekondigd na de introductie van het bord op CES 2014. In 2019 werd een link toegevoegd om Wolfram-bibliotheken compatibel te maken met de Unity-game-engine, waardoor game-ontwikkelaars toegang krijgen tot de functies op hoog niveau van de taal.
            </p>
        </div>
    </div>
    <?php include("../footer-talen.php");?>
</body>
</html>